
### Use Case: User 
No | Use Case | Deskripsi | Score
---|---|---|---
1 | Menjelajahi Map | Player mengekplorasi map yang ada. | 10
2 | Menyerang Musuh | Player dapat menyerang musuh yang tersedia. | 10
3 | Menerima Serangan dari musuh | Selain dapat menyerang Player juga dapat di serang oleh musuh. | 9
4 | Memiliki nyawa | Player mempunyai nyawa yang dimana akan menghilang saat menerima serangan dari musuh, Saat nyawa sudah habis maka player akan meninggoy. | 9
5 | Melakukan Pertarungan | Player dapat bertarung dengan musuh yang tersedia yang dimana musuh sudah di lengkapi dengan kecerdasan yaitu dapat mengejar player. | 10
6 | Bermain Bersama Pemain Lain | Pengguna dapat bermain bersama dengan pemain lain dalam pertarungan melawan musuh dalam dunia fantasy. | 9
7 | Melihat coin | Saat sudah mengalahkan musuh akan mendapatkan 20 coin. | 7
8 | Menghancurkan alam | Menyerang dadaunan juga termasuk merusak alam. | 7
9 | Mengikuti Event | Pengguna dapat mengikuti event khusus yang diselenggarakan dalam permainan. | 8
10 | Berinteraksi dengan NPC | Pengguna dapat berinteraksi dengan karakter non-playable (NPC) dalam permainan untuk mendapatkan informasi, tugas, atau hadiah. | 9
11 | Menjelajahi Dunia Terbuka | Pengguna dapat menjelajahi dunia terbuka dalam permainan untuk menemukan lokasi baru, sumber daya, atau fitur tersembunyi. | 10
12 | Membangun dan Mengelola Rumah | Pengguna dapat membangun dan mengelola rumah pribadi dalam permainan dengan mengatur dekorasi, furniture, dan fitur lainnya. | 8
13 | Mengumpulkan Barang Langka | Pengguna dapat mencari dan mengumpulkan barang-barang langka atau koleksi dalam permainan. | 9
14 | Berpartisipasi dalam Pertandingan PvP | Pengguna dapat berpartisipasi dalam pertandingan PvP (Player vs Player) untuk menguji kemampuan dan strategi mereka melawan pemain lain. | 10
15 | Membuka Akses ke Area Tersembunyi | Pengguna dapat menemukan dan membuka akses ke area tersembunyi dalam permainan dengan menyelesaikan tantangan atau memenuhi persyaratan tertentu. | 8
16 | Mengendarai Kendaraan | Pengguna dapat menggunakan kendaraan seperti mobil, pesawat terbang, atau hewan tunggangan dalam permainan. | 9
17 | Berdagang dan Berbisnis | Pengguna dapat melakukan aktivitas perdagangan, membuka toko, atau menjalankan bisnis dalam permainan. | 7
18 | Menyelesaikan Puzzle dan Tantangan | Pengguna dapat menyelesaikan berbagai puzzle atau tantangan logika yang ada dalam permainan. | 8
19 | Mengembangkan Keahlian Karakter | Pengguna dapat mengembangkan keahlian dan kemampuan karakter mereka melalui latihan, pelatihan, atau pengalaman. | 9
20 | Memperoleh Prestasi dan Gelar | Pengguna dapat memperoleh prestasi dan gelar khusus berdasarkan pencapaian mereka dalam permainan. | 7

### Use Case: Manajemen Perusahaan
No | Use Case | Deskripsi | Score
---|---|---|---
1 | Melihat Kapan player daftar | Tim manajemen perusahaan dapat Melihat kapan saat player daftar. | 10
2 | Mengelola LeaderBoard | Tim manajemen perusahaan dapat mengelola informasi LeaderBoard yang menggunakan game. | 9
3 | Menambah Map | Tim manajemen dapat memperluas map yang dapat di jelajah. | 10
4 | Mengelola Event dan Konten Khusus | Tim manajemen perusahaan dapat membuat dan mengelola event khusus, konten baru, atau pembaruan dalam permainan. | 9
5 | Menambah Jenis Musuh | Menambah musuh juga dapat dilakukan untuk manmbah daya tarik pemain supaya tidak bosan. | 10
6 | Mengelola Tim Pengembang | Tim manajemen perusahaan dapat mengelola tim pengembang game, termasuk rekrutmen, alokasi sumber daya, dan jadwal pengembangan. | 8
7 | Mengatur Sistem Keamanan | Tim manajemen perusahaan dapat mengatur dan memantau sistem keamanan dalam permainan untuk melindungi data pengguna dan mencegah penyalahgunaan. | 9
8 | Melakukan Riset Pasar | Tim manajemen perusahaan dapat melakukan riset pasar untuk mengidentifikasi tren, kebutuhan pengguna, atau peluang pengembangan permainan. | 8
9 | Mengelola Monetisasi | Tim manajemen perusahaan dapat mengatur strategi monetisasi game, termasuk iklan, pembelian dalam aplikasi, atau model bisnis lainnya

### Use Case: Direksi Perusahaan
No | Use Case | Deskripsi | Score
---|---|---|---
1 | Dashboard Kinerja | Direksi perusahaan dapat melihat data kinerja game secara keseluruhan melalui dashboard. | 10
2 | Analisis Data | Direksi perusahaan dapat menganalisis data terkait dengan game TCG Genshin Impact. | 10
3 | Evaluasi Keuangan | Direksi perusahaan dapat melakukan evaluasi keuangan untuk memantau pendapatan, biaya, dan profitabilitas game. | 9
4 | Pengembangan Strategi | Direksi perusahaan dapat mengembangkan strategi jangka panjang dan visi untuk pengembangan game. | 10
5 | Menetapkan Target Kinerja | Direksi perusahaan dapat menetapkan target kinerja untuk tim pengembang dan departemen terkait dalam mencapai tujuan bisnis. | 9
6 | Manajemen Risiko | Direksi perusahaan dapat mengidentifikasi dan mengelola risiko yang terkait dengan pengembangan dan operasional game. | 8
7 | Kolaborasi dengan Mitra | Direksi perusahaan dapat melakukan kolaborasi dengan mitra strategis untuk pengembangan game dan pemasaran. | 9
8 | Penilaian Kualitas | Direksi perusahaan dapat melakukan penilaian kualitas terhadap konten, fitur, dan pengalaman pengguna dalam game. | 8
9 | Pengembangan Ekspansi | Direksi perusahaan dapat mengembangkan rencana ekspansi, termasuk pengembangan versi mobile, ekspansi pasar, atau perluasan konten. | 10
10 | Komunikasi dengan Pemangku Kepentingan | Direksi perusahaan dapat berkomunikasi dengan pemangku kepentingan, seperti investor, pemegang saham, atau media, untuk memberikan pembaruan dan informasi tentang perkembangan game. | 9
